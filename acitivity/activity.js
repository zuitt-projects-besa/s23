
/*ADD 5 USERS*/

db.users.insertMany([
			{
				"firstName" : "John",
				"lastName" : "Lennon",
				"email" : "johnlennon@mail.com",
				"password" : "password1",
				"isAdmin" : false
			},
			{
				"firstName" : "Paul",
				"lastName" : "McCartney",
				"email" : "paulmccartney@mail.com",
				"password" : "password2",
				"isAdmin" : false
			},
			{
				"firstName" : "Ringo",
				"lastName" : "Star",
				"email" : "ringostar@mail.com",
				"password" : "password3",
				"isAdmin" : false
			},
			{
				"firstName" : "George",
				"lastName" : "Harrison",
				"email" : "georgeharrison@mail.com",
				"password" : "password4",
				"isAdmin" : false
			},
			{
				"firstName" : "Pete",
				"lastName" : "Best",
				"email" : "petebest@mail.com",
				"password" : "password5",
				"isAdmin" : false
			}

		]);


/*ADD 3 COURSES*/

db.courses.insertMany([
		{
			"name" : "CSS",
			"price" : 1500,
			"isActive" : false
		},
		{
			"name" : "HTML",
			"price" : 1000,
			"isActive" : false
		},
		{
			"name" : "Javascript",
			"price" : 2000,
			"isActive" : false
		}
	]);




/*READ*/

db.users.find({"isAdmin" : false});

/*UPDATE*/

db.users.updateOne({}, {$set: {"isAdmin" : true}});
db.courses.updateOne({"name": "CSS"}, {$set: {"isActive" : true}});

/*DELETE*/

db.courses.deleteMany({"isActive" : false});